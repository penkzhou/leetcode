package com.penkzhou.leetcode;

import java.util.Hashtable;

/*
 *
 * Given an array of integers, find two numbers such that they add up to a specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target, where index1 must be less than index2. Please note that your returned answers (both index1 and index2) are not zero-based.

You may assume that each input would have exactly one solution.

Input: numbers={2, 7, 11, 15}, target=9
 */

public class TwoNumberSum {
	public static int[] twoSum(int[] numbers, int target) {
		int[] index = new int[2];
        Hashtable ht = new Hashtable();
        for(int i=0;i<numbers.length;i++){
            ht.put(numbers[i],i);
        }
        for(int i=0;i<numbers.length;i++){
            if(ht.get(target-numbers[i])!=null){
                int target_index = (Integer)ht.get(target-numbers[i]);
                if(target_index != i){
                    index[0] = (i> target_index ? target_index:i)+1;
                    index[1] = (i< target_index ? target_index:i)+1;
                    break;
                }
            }
        }
        return index;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] nums = {1,3,6,8,9};
		System.out.println(twoSum(nums, 9)[0]+" ");
	}

}
