package com.penkzhou.leetcode;

import java.util.ArrayList;

/**
 * Given an input string, reverse the string word by word.
 * For example,
 * Given s = "the sky is blue",
 * return "blue is sky the".
 * Clarification:
 * What constitutes a word?
 * A sequence of non-space characters constitutes a word.
 * Could the input string contain leading or trailing spaces?
 * Yes. However, your reversed string should not contain leading or trailing spaces.
 * How about multiple spaces between two words?
 * Reduce them to a single space in the reversed string.
 * 
 * 
 * @author Administrator
 *
 */
public class ReverseWords {

	public static void main(String[] args) {
	}
	
	public String reverseWords(String s) {
    	s = s.trim();
    	if (s.equals("")) {
			return s;
		}
    	if (!s.contains(" ")) {
			return s;
		}
    	StringBuffer sBuffer = new StringBuffer();
    	String array[] = s.split(" ");
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			if (!array[i].equals("")) {
				stringBuffer.insert(0, array[i]+" ");
			}
		}
		int length = stringBuffer.length();
		stringBuffer = stringBuffer.delete(length-1, length);
		return stringBuffer.toString();
    }

}
