package com.penkzhou.leetcode;

import java.util.*;

/**
 *  https://oj.leetcode.com/problems/path-sum-ii/
 * @author Administrator
 *            5
             / \
            4   8
           /   / \
          11  13  4
         /  \    / \
        7    2  5   1
 */

public class Path_Sum_II {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeNode root = new TreeNode(5);
		TreeNode root1 = new TreeNode(4);
		TreeNode root2 = new TreeNode(8);
		TreeNode root3 = new TreeNode(11);
		TreeNode root4 = new TreeNode(13);
		TreeNode root5 = new TreeNode(4);
		TreeNode root6 = new TreeNode(7);
		TreeNode root7 = new TreeNode(2);
		TreeNode root8 = new TreeNode(5);
		TreeNode root9 = new TreeNode(1);
		root.left = root1;
		root.right = root2;
		root1.left = root3;
		root2.left = root4;
		root2.right = root5;
		root3.left = root6;
		root3.right = root7;
		root5.left = root8;
		root5.right = root9;
		new Path_Sum_II().pathSum(root, 22);
	}
	
	public List<List<Integer>> pathSum(TreeNode root, int sum) {
		int tempSum = 0;
		LinkedList<Integer> pathIntegers = new LinkedList<>();
		printArray(root, pathIntegers,sum);
        return null;
    }
	
	public void printArray(TreeNode node,LinkedList<Integer> path, int sum){
		if (node == null) {
			return;
		}
		path.add(node.val);
		if (node.left == null && node.right == null && sum == sumLinkedList(path)) {
			System.out.println(path);
			return;
		}
		printArray(node.left, new LinkedList<>(path), sum);
		printArray(node.right, new LinkedList<>(path), sum);
	}
	
	public int sumLinkedList(LinkedList<Integer> list) {
		int sum = 0 ;
		for (Integer item : list) {
			sum += item;
		}
		return sum;
	}
	
	
}
class TreeNode {
	int val;
	TreeNode left;
	TreeNode right;
	public TreeNode(int x) { 
		val = x; 
		}
 }



